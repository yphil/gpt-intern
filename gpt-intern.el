(require 'json)
(require 'color)

(defgroup gpt-intern nil
  "GPT frontend."
  :group 'convenience
  :prefix "gpt-intern-")

(defcustom gpt-intern-max-tokens 300
  "The API will not return more than a certain number of tokens."
  :type 'integer)

(defcustom gpt-intern-temperature 0.0
  "Temperature value for the API request."
  :type 'float)

(defcustom gpt-intern-model "gpt-3.5-turbo"
  "Model to be used for the API request."
  :type '(choice (const :tag "GPT-3.5 Turbo" "gpt-3.5-turbo")
                 (const :tag "Text Davinci 003" "text-davinci-003")
                 (string :tag "Other")))

(defcustom gpt-intern-api-key-file "~/.metazdoctor"
  "The file containing your API key.
A regular text file containing only your API key ; Get one at https://openai.com/."
  :type 'string)

(defcustom gpt-intern-role-system-content "You are a laconic assistant. You reply with brief, to-the-point answers with no elaboration."
  "Content of the system role message."
  :type 'string)

(defcustom gpt-intern-api-url "https://api.openai.com/v1/chat/completions"
  "The URL for the OpenAI API."
  :type 'string
  :group 'gpt-intern)


(defface gpt-intern-face-answer
  `((t :inherit default :foreground ,(color-darken-name (face-foreground 'default) 20)))
  "Face for answers. It's 20% darker than the default face."
  :group 'gpt-intern)

(defface gpt-intern-face-question
  `((t :inherit default :foreground ,(color-lighten-name (face-foreground 'default) 20)))
  "Face for questions. It's 20% lighter than the default face."
  :group 'gpt-intern)

(defvar gpt-intern-api-key nil
  "The variable to store the API key.")

;;;###autoload
(defun gpt-intern--api-key-from-file (file)
  "Set the gpt-intern-api-key variable from the FILE if it is not already set.
If the variable is not set and the file does not exist, display a warning message."
  (unless gpt-intern-api-key
    (if (file-exists-p file)
        (with-temp-buffer
          (insert-file-contents file)
          (setq gpt-intern-api-key (buffer-string)))
      (message "Warning: file not found: %s" file))))

(defun create-message (role content)
  "Create a message in the format expected by GPT-4."
  ;; Return a message in the format ((role . role) (content . content))
  `((role . ,role) (content . ,content)))

(defvar gpt-intern-buffer "Gpt Intern  - press q to close"
  "Name of the buffer used to store the results of an OpenAI API query.")

(defvar messages '()
  "List of messages to maintain the conversation history.")

(defun gpt-intern--display-gpt-chat-buffer (role message)
  "Append the message to the gpt-intern-buffer, prefixed with 'Q:' or 'R:'.
   The point is placed at the bottom of the buffer after inserting the message."
  (push (create-message role message) messages)
  (with-current-buffer (get-buffer-create gpt-intern-buffer)
    (view-mode -1)
    (goto-char (point-max))
    (let ((start (point)))
      (insert (if (string= role "user") "Q: " "R: ") message "\n\n")
      (let ((end (point)))
        (put-text-property start end 'face (if (string= role "user") 'gpt-intern-face-question 'gpt-intern-answer-face))))
    (set-window-point (get-buffer-window) (point-max))
    (view-mode 1)
    (unless (string= role "user")  ;; Only prompt for a new question after displaying an assistant's message
      (gpt-intern-prompt))))

(defun gpt-intern--parse-buffer ()
  "Parse the gpt-intern-buffer and return a list of messages."
  (with-current-buffer gpt-intern-buffer
    (goto-char (point-min))
    (let ((messages '()))
      (while (not (eobp))
        (let ((line (buffer-substring-no-properties (line-beginning-position) (line-end-position))))
          (when (string-match "^\\(Q: \\|R: \\)\\(.*\\)" line)
            (let* ((role (if (string= (match-string 1 line) "Q: ") "user" "assistant"))
                   (content (match-string 2 line))
                   (message `(("role" . ,role) ("content" . ,content))))
              (push message messages))))
        (forward-line))
      (message "Parsed messages: %s" (json-encode (nreverse messages))))))

(defun gpt-intern-prompt (&optional beg end query)
  "Prompt for a question, prepend selected region if any, and send to GPT API.
When called interactively with a region, BEG and END specify the region bounds."
  (interactive (if (use-region-p) (list (region-beginning) (region-end) nil) (list nil nil nil)))
  (with-local-quit
    (let* ((buffer (get-buffer-create gpt-intern-buffer))
           (region-text (when beg (buffer-substring-no-properties beg end))))
      (with-current-buffer buffer
        (rename-buffer gpt-intern-buffer)
        (set-buffer-file-coding-system 'utf-8))
      (pop-to-buffer buffer)  ;; Move this line up
      (let ((user-input (or query (read-string "Ask: "))))
        (when region-text
          (setq user-input (concat region-text "\n" user-input)))
        (message "Parsed messages in prompt: %s" (gpt-intern--parse-buffer))
        (gpt-intern--display-gpt-chat-buffer "user" user-input)
        (message "Current messages: %s" (json-encode messages))
        (gpt-intern--query-api (nreverse messages))))))

(defun gpt-intern--parse-response (status)
  "Parse the HTTP response from the GPT API.
  If there's an error in the status, display it.
  Try to parse the JSON response.
  If there's an error parsing the JSON, display it."
  (if (eq (car status) :error)
      (let* ((error-object (cdr status))
             (error-message (car error-object)))
        (message "Error: %s" error-message))
  
    (goto-char url-http-end-of-headers)
    (let ((json-object-type 'plist)
          (json-array-type 'list)
          (json-key-type 'keyword))
      (condition-case err
          (let* ((response (json-read))
                 (choices (plist-get response :choices))
                 (first-choice (car choices))
                 (message (plist-get first-choice :message))
                 (content (plist-get message :content))
                 (usage (plist-get response :usage))
                 (prompt-tokens (plist-get usage :prompt_tokens))
                 (completion-tokens (plist-get usage :completion_tokens))
                 (total-tokens (plist-get usage :total_tokens)))
            (setq-default global-mode-string (format "Prompt: %s, Completion: %s, Total: %s" prompt-tokens completion-tokens total-tokens))  ;; Set the message in the global-mode-string
            (message "Prompt: %s, Completion: %s, Total: %s" prompt-tokens completion-tokens total-tokens)
            (gpt-intern--display-gpt-chat-buffer "assistant" content))
        (error
         (message "Error parsing JSON response: %s" (error-message-string err)))))))


(defun gpt-intern--query-api (messages)
  "Sends a POST request to the GPT API with the given MESSAGES."
  (let* ((url-request-method "POST")
         (url-request-extra-headers `(("Content-Type" . "application/json; charset=utf-8")
                                      ("Authorization" . ,(format "Bearer %s" gpt-intern-api-key))))
         (request-body `(("model" . ,gpt-intern-model)
                         ("temperature" . ,gpt-intern-temperature)
                         ("messages" . ,messages)))  ;; Use the reversed messages directly
         (url-request-data (encode-coding-string (json-encode request-body) 'utf-8))
         (coding-system-for-read 'utf-8))
    (message "Request Body: %s" (json-encode request-body))
    (url-retrieve gpt-intern-api-url
                  (lambda (status)
                    (gpt-intern--parse-response status)))))  ;; Use the correct callback function

;;;###autoload
(add-hook 'after-init-hook (lambda () (gpt-intern--api-key-from-file gpt-intern-api-key-file)))

(provide 'gpt-intern)
